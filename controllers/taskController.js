const Task = require('../models/task');

// Get all tasks
module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=>{
		return result;
	})
}

// Add task
module.exports.createTask = (requestBody)=>{
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error)=>{
		if (error) {
			console.log(error);
		} else {
			return task;
		}
	});
}

// Get specific task
// Get the id from the url
// Use that id to find it in the tasks collection
// Use Task model. Task.findById()
module.exports.getTask = (taskId)=>{
	// The 'findById' Mongoose method will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result,error)=>{
		if (error) {
			console.log(error);
			return false;
		// Find successful, returns the task object back to the client/Postman
		}else{
			return result;
		}
	})
}


// Delete specific task
module.exports.deleteTask = (taskId)=>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if (err) {
			console.log(error);
			return false;
		}else{
			return removedTask;
		}
	})
}