const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');

// Routes to get all tasks
router.get('/',(req, res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


// Route to post or add a task
router.post('/',(req, res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to get specific task
router.get('/:id',(req, res)=>{
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to delete specific task
router.delete('/:id',(req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Use 'module.exports' to export the router object to use in the 'app.js'
module.exports = router;